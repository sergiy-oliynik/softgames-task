import {CARD_WIDTH, CARD_HEIGHT, FONT_DESYREL, SPECIAL_CHARACTERS} from '../const';
import Stack from '../view/Stack';
import Text from '../view/Text';
import Button from '../view/Button';
import Particle from '../view/Particle'

const Point = PIXI.Point;
const Container = PIXI.Container;
const Application = PIXI.Application;
const doc = document;
const win = window;

let emitter;

export default class App extends Application {
    constructor() {
        const view = doc.getElementById('game');

        const w = win.innerWidth;
        const h = win.innerHeight;
        const ratio = window.devicePixelRatio || 1;

        super({
            width: w,
            height: h,
            view: view,
            resolution: ratio,
            autoStart: true,
            autoResize: true
        });

        this.stats = new Stats();
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.top = '0';

        doc.body.appendChild(this.stats.domElement);

        window.addEventListener('resize', (event) => {
            this.renderer.resize(win.innerWidth, win.innerHeight);
            this.updatePosition();
        });
    }

    start() {
        super.start();

        // cards
        this._leftStack = new Stack();
        this._leftStack.on('finish', this.onAnimationFinish, this);
        this._leftStack.initCards();
        this.stage.addChild(this._leftStack);

        this._rightStack = new Stack();
        this._rightStack.on('finish', this.onAnimationFinish, this);
        this.stage.addChild(this._rightStack);

        // font
        Text.registerCharacters(FONT_DESYREL, SPECIAL_CHARACTERS);

        this._text = new Text();
        this._text.visible = false;
        // Another way to make custom text
        // this._text.replace('Do you want to get 100 {diamond} or 100 {coin}?', SPECIAL_CHARACTERS);
        this._text.position.set(150, 200);
        this.stage.addChild(this._text);

        // particles
        this._container = new Container();
        this.stage.addChild(this._container);
        this._particle = new Particle(this._container);

        // buttons
        this._button01 = new Button();
        this._button01.on('pointertap', this.onButtonCards, this);
        this._button01.visible = false;
        this._button01.position.set(150, 100);
        this._button01.text = 'Cards';
        this.stage.addChild(this._button01);

        this._button02 = new Button();
        this._button02.on('pointertap', this.onButtonFont, this);
        this._button02.position.set(150, 130);
        this._button02.text = 'Font';
        this._button02.visible = true;
        this.stage.addChild(this._button02);

        this._button03 = new Button();
        this._button03.on('pointertap', this.onButtonFire, this);
        this._button03.position.set(150, 160);
        this._button03.text = 'Particle';
        this._button03.visible = true;
        this.stage.addChild(this._button03);

        this.updatePosition();
        this.animate();
    }

    updatePosition() {
        this._leftStack.position.set(0, 50);
        this._rightStack.position.set(win.innerWidth - CARD_WIDTH, win.innerHeight - CARD_HEIGHT);
    }

    render() {
        this.stats.begin();
        PIXI.tweenManager.update();
        this._particle.update();
        super.render();
        this.stats.end();
    }

    animate() {
        this._leftStack.animate(this._rightStack);
    }

    onAnimationFinish() {
        this.stage.children.reverse();// makes active stack above another one
    }

    onButtonCards() {
        this._button01.visible = false;
        this._button02.visible = true;
        this._button03.visible = true;

        this._leftStack.visible = true;
        this._rightStack.visible = true;

        this._text.visible = false;

        this._particle.hide();
    }

    onButtonFont() {
        this._button01.visible = true;
        this._button02.visible = false;
        this._button03.visible = true;

        this._leftStack.visible = false;
        this._rightStack.visible = false;

        this._text.randomize();
        this._text.visible = true;

        this._particle.hide();
    }

    onButtonFire() {
        this._button01.visible = true;
        this._button02.visible = true;
        this._button03.visible = false;

        this._leftStack.visible = false;
        this._rightStack.visible = false;

        this._text.visible = false;

        this._particle.show();
    }
}
