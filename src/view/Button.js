import Text from './Text';

const Sprite = PIXI.Sprite;
const Texture = PIXI.Texture;

export default class Button extends Sprite {
    constructor() {
        super(Texture.fromFrame('button'));

        this._text = null;
        this._bitmap = null;

        this.interactive = true;
        this.anchor.set(0.5, 0.5);

        this._bitmap = new Text("", {font: '20px Desyrel', align: 'center'}, false);
        this._bitmap.anchor.set(0.5, 0.5);
        this.addChild(this._bitmap);
    }

    set text(value) {
        this._bitmap.text = value;
    }
}