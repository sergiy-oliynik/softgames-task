import { CARD_LINK } from '../const';

const Texture = PIXI.Texture;
const Sprite = PIXI.Sprite;

export default class Card extends Sprite {
    constructor() {
        super(Texture.fromFrame(CARD_LINK));
    }
}