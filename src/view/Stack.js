import { CARDS_COUNT, CARD_PADDING } from '../const';
import Card from '../view/Card';
import Timer from '../data/Timer';
import TweenUtil from '../util/TweenUtil';

const Container = PIXI.particles.ParticleContainer;
const Point = PIXI.Point;

export default class Stack extends Container {
    constructor() {
        super();

        /**
         * @type {Timer}
         * @private
         */

        this._timer = null;

        /**
         * @type {boolean}
         * @private
         */

        this._animation = false;

        /**
         * @type {number}
         * @private
         */

        this._started = 0;
    }

    initCards() {
        for (let i = 0; i < CARDS_COUNT; i++) {
            const card = new Card();
            card.position = this.calculateCoords();
            this.addChildAt(card, 0);
        }
    }

    clear() {
        this._started = 0;
        this._animation = false;
        this._timer = null;
    }

    calculateCoords(toTop = false) {
        const y = (toTop ? -1 : 1) * this.children.length * CARD_PADDING;

        return new Point(0, y);
    }

    /**
     * @param {Stack} stack - another stack
     * @param {boolean} reverse - if true - cards are coming back
     */

    animate(stack, reverse = false) {
        if (this._animation) {
            console.log('Process has already started');
            return;
        }

        this._animation = true;

        console.log('Start animation: ' + this._animation);

        this._timer = new Timer();
        this._timer.on('second', () => {
            this.move(stack, reverse);
        }, this);
        this._timer.start();
    }

    move(target, reverse) {
        const len = this.children.length;

        if (!len) {
            this._timer.stop();
            this._timer.removeAllListeners('second');

            console.log('Stack is empty');
            return;
        }

        const card = this.children.pop();
        const pos = card.position;
        const global = this.toGlobal(pos);
        const local = target.toLocal(global);

        const to = target.calculateCoords(!reverse);

        card.position = local;

        target.addChild(card);

        this._started++;

        TweenUtil.move(card, to, 2, () => {
            --this._started;

            // all children started and finished an action
            if (!this.children.length && !this._started) {
                console.log('Process has finished');

                this.emit('finish', this);
                this.clear();

                target.animate(this, !reverse);
            }
        }, this);
    }
}