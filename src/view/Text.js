import MathUtil from '../util/MathUtil';
import { FONT_DESYREL } from '../const';

const BitmapText = PIXI.extras.BitmapText;
const Texture = PIXI.Texture;

export default class Text extends BitmapText {
    static registerCharacters(fontName, characters) {
        const font = BitmapText.fonts[fontName];

        if (!font) {
            return;
        }

        const chars = font['chars'];
        const tmp = chars[65];
        const tmpTexture = tmp.texture;
        const tmpFrame = tmpTexture.frame;

        for (var textureName in characters) {
            const charCode = characters[textureName];
            const texture = Texture.fromFrame(textureName);
            const frame = texture.frame;

            chars[charCode] = {
                kerning: {},
                texture: texture,
                xAdvance: frame.width,
                xOffset: 1,
                yOffset: tmp.yOffset + (tmpFrame.height - frame.height) / 2 + 5
            };
        };
    }

    constructor(text = "", style = null, random = true) {
        if (style == null) {
            const size = MathUtil.randomInt(20, 80) + 'px';
            style = {font: size + ' ' + FONT_DESYREL, align: 'center'};
        }

        super(text, style);

        if (random) {
            this.randomize();
        }
    }

    randomize() {
        let text = "";

        for (let i = 0; i < 3; i++) {
            const rnd = Math.random();

            switch (true) {
                case (rnd < 0.33):
                    text += String.fromCharCode(5000);
                    break;
                case (rnd < 0.66):
                    text += String(MathUtil.randomInt(100, 1000));
                    break;
                default:
                    text += String.fromCharCode(5001);
                    break;
            }
        }

        this.text = text;
    }

    replace(str, data) {
        try {
            for (var i in data) {
                while (str.search(new RegExp('{' + i + '}', 'i')) != -1)
                    str = str.replace(new RegExp('{' + i + '}', 'i'), String.fromCharCode(data[i]));
            }
        }
        catch (e) {
        }

        this.text = str;
    }
}