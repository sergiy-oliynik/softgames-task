const Emitter = PIXI.particles.Emitter;
const Texture = PIXI.Texture;

export default class Particle {
    constructor(parent) {
        this._parent = parent;

        this.emitter = new Emitter(parent, [Texture.fromFrame('fire')], {
            "alpha": {
                "start": 1,
                "end": 0
            },
            "scale": {
                "start": 0.2,
                "end": 0.5,
                "minimumScaleMultiplier": 3
            },
            "color": {
                "start": "#c9b418",
                "end": "#f2573f"
            },
            "speed": {
                "start": 500,
                "end": 500,
                "minimumSpeedMultiplier": 2
            },
            "acceleration": {
                "x": -3,
                "y": 1
            },
            "maxSpeed": -4,
            "startRotation": {
                "min": 275,
                "max": 285
            },
            "noRotation": false,
            "rotationSpeed": {
                "min": 50,
                "max": 50
            },
            "lifetime": {
                "min": 0.2,
                "max": 0.001
            },
            "blendMode": "normal",
            "frequency": 0.001,
            "emitterLifetime": -1,
            "maxParticles": 10,
            "pos": {
                "x": 5,
                "y": 0
            },
            "addAtBack": false,
            "spawnType": "circle",
            "spawnCircle": {
                "x": 0,
                "y": 0,
                "r": 5
            }
        });

        this.elapsed = Date.now();
        this.emitter.updateOwnerPos(130, 350);
        this.emitter.emit = true;
        this.hide();
    }

    show() {
        this._parent.visible = true;
    }

    hide() {
        this._parent.visible = false;
    }

    update() {
        var now = Date.now();
        if (this.emitter)
            this.emitter.update((now - this.elapsed) * 0.001);
        this.elapsed = now;
    }
}