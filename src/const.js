export const CARD_LINK = 'card';
export const CARDS_COUNT = 20;
export const CARD_PADDING = 20;
export const CARD_WIDTH = 40;
export const CARD_HEIGHT = 57;

export const FONT_DESYREL = 'Desyrel';
export const SPECIAL_CHARACTERS = {
    diamond: 5000,
    coin: 5001
};