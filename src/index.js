import { CARD_LINK } from './const';
import App from './main/App';

window.onload = () => {
    const loader = PIXI.loader;
    loader.add(CARD_LINK, 'asset/img/sprite.jpg');
    loader.add('diamond', 'asset/img/diamond.png');
    loader.add('coin', 'asset/img/coin.png');
    loader.add('button', 'asset/img/button.png');
    loader.add('fire', 'asset/img/fire.png');
    loader.add('font', 'asset/font/desyrel.xml');
    loader.load(() => {
        const app = new App();
        // app.start();
    });
}