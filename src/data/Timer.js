const EventEmitter = PIXI.utils.EventEmitter;

export default class Timer extends EventEmitter {
    constructor() {
        super();

        this._ticker = new PIXI.ticker.Ticker();
        this._frame = 0;
    }

    start() {
        if (this._ticker.started) {
            return;
        }

        this._ticker.add(this.onFrame, this);
        this._ticker.start();
    }

    stop() {
        if (!this._ticker.started) {
            return;
        }

        this._ticker.stop();
    }

    onFrame(delta) {
        this._frame += delta;

        if (this._frame >= 60) {
            this._frame = 0;

            this.emit('second');
        }
    }
}