const Tween = PIXI.tween.Tween;

export default class TweenUtil {
    static add(tween) {
        PIXI.tweenManager.addTween(tween);
    }
    static remove(tween) {
        PIXI.tweenManager.removeTween(tween);
    }

    static move(target, to, time, cb, context) {
        const tween = new Tween(target, null);

        this.add(tween);

        tween.time = time * 1000; // milliseconds to seconds
        tween.from({x: target.x, y: target.y});
        tween.to({x: to.x, y: to.y});
        tween.on('end', () => {
            this.remove(tween);

            if (cb && context) {
                cb.call(context);
            }
        });
        tween.start();

        return tween;
    }
}