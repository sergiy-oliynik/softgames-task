export default class MathUtil {
    static randomInt(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
}